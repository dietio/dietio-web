package com.dietio.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DietiowebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DietiowebApplication.class, args);
	}

}
